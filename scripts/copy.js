module.exports = function(context) {
var fs = require('fs');
  let copy = ['node_modules/pouchdb/dist/pouchdb.js',
	      'node_modules/strophe.js/strophe.js',
	      'node_modules/strophejs-plugins/muc/strophe.muc.js',
	      'node_modules/strophejs-plugins/si-filetransfer/strophe.si-filetransfer.js',
	      'node_modules/strophejs-plugins/ibb/strophe.ibb.js',
	      'node_modules/fingerprintjs2/dist/fingerprint2.min.js'
	     ];
  let dest_path = 'www/js/';

  for(var i=0;i<copy.length;i++)
  {
    let name = copy[i].split('/').pop();;
    fs.createReadStream(copy[i]).pipe(fs.createWriteStream(dest_path + '/' + name));
  }
};
//module.exports();


