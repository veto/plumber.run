<?php

class IndexController extends ControllerBase
{
  public function indexAction()
  {
    $this->assets->addCss("css/navslide.css");
    $this->assets->addCss("css/screen.css");
  }
}

