var Database = function()
{
  var self = this;
  this.name = 'user_db';
  self.db = false;
  this.init = function()
  {
    self.db = new PouchDB(this.name);
    self.db.get('profile', function(err, doc) {
    if(err)
    { 
      if(err['message'] == 'missing')
      {
        self.db.put({
	  _id:'profile',
	  password:GPW.pronounceable(10)
	});
      }
    }
    }) ;

  };

  this.update_user_profile = function(app,data)
  {
    self.db.get(data['_id']).then(function(doc) {
      data['_rev'] = doc._rev;
      return self.db.put(data);
    }).then(function(response) {
    // handle response
    }).catch(function (err) {
      return self.db.put(data);      
    });
  };


  this.fill_profile_form = function(app)
  {
    this.db.get('profile').then(function (doc) {
      app.form.fillFromData("#profile",doc);
    }).catch(function (err) {

    });
  };


  this.set_profile = function(app)
  {
    this.db.get('profile').then(function (doc) {
      app.data['profile'] =  doc;
    }).catch(function (err) {

    });
  };


  this.get_doc = function(app,_id,_key)
  {
    self.db.get(_id, function(err, doc) {
    if (err) { return console.log(err); }
      app.data['user']['password'] =  doc[_id][_key];
   });
  };

  this.update_doc = function(doc)
  {
   self.db.put(doc, function(err, response)
   {
    if (err) {console.log(err);}
   });


  };

  this.init();
};


