var db = new Database();
var $$ = Dom7;
// Framework7 App main instance
var app  = new Framework7({
  root: '#app', // App root element
  id: 'io.framework7.testapp', // App bundle ID
  name: 'Plumber', 
  language: 'en', 
  theme: 'auto', 
  virtual: true,
  // App root data
  data: function () {
    return {
      virutal: false,
      profile: db.set_profile(this)
    };
  },
  methods: {
    helloWorld: function () {
      app.dialog.alert('Hello World!');
    },
  },
  routes: routes,

  on: {
    init: function () {
      console.log('App initialized');
      //pouch.set_user_profile();
      //pouch.show_user_profile(this);
    },
    pageInit: function () {
      //console.log('Page initialized');
    }
  }

});


// Init/Create views
var homeView = app.views.create('#view-index', {
  url: '/'
});
var catalogView = app.views.create('#view-catalog', {
  url: '/catalog/'
});
var your_business_View = app.views.create('#view-your_business', {
  url: '/your_business/'
});

var chats_view = app.views.create('#view-chats', {
  url: '/chats/'
});

var contacts_view = app.views.create('#view-contacts', {
  url: '/contacts/'
});


// Login Screen Demo
$$('#my-login-screen .login-button').on('click', function () {
  var username = $$('#my-login-screen [name="username"]').val();
  var password = $$('#my-login-screen [name="password"]').val();

  // Close login screen
  app.loginScreen.close('#my-login-screen');

  // Alert username and password
  app.dialog.alert('Username: ' + username + '<br>Password: ' + password);
});


$$(document).on('page:afterin', function (e) {
  if(e.target.id === 'catalog')
  {
   set_uuid(app);      
   console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');
   console.log(app.data['profile']);
   console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx');

   //set_password(app)
   //app.data['user']['password'] = 
   //alert(app.data['user']['password'] + '---' + app.data['uuid']);
  }
});

$$(document).on('page:beforein', function (e) {

  if(e.target.id === 'contacts')
  {
    var contact_searchbar = app.searchbar.create(
    {
      el: '#contact_searchbar',
      searchIn: '.item-title',
      on:
      {
        search(sb, query, previousQuery)
        {
          console.log(query, previousQuery);
        }
     }
    }); 
  }
  else if(e.target.id === 'chats')
  {
  }

  else if(e.target.id === 'your_business')
  {
    db.fill_profile_form(app);
    $$('#save').on('click', function()
    {
      var form_data = app.form.convertToData('#profile');
      db.update_user_profile(app,form_data);
    });
  }

  else if(e.target.id === 'catalog')
  {

  }

});


function set_uuid(app)
{




  if(app.data['profile'].hasOwnProperty('uuid') === false)
  {
    if( device.uuid == null && device.isVirtual == false)
    {
      new Fingerprint2().get(function(r,c)
      {
        app.data['profile']["uuid"] = r;
        db.update_doc(app.data['profile']);
      });
    }
    else
    {
      app.data['profile']["uuid"] = device.uuid;
      db.update_doc(app.data['profile']);
    }

  }

  
}









